####################################################
Welcome to the PyScatman documentation!
####################################################


Please, read the documentation carefully and report any issue to alcolombo@phys.ethz.ch .
Thanks for your help and support!

.. toctree::
   :maxdepth: 4
   
   self 

   intro
   install
   quickstart
   settings
   shapes
   detectors
   pattern
..    :hidden:
   

..    :caption: Contents:

 
.. 
.. Indices and tables
.. ==================
.. 
.. 
.. 
.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
